# dev.py
from conf.base import *

SECRET_KEY = '=^6)f6&qoh$0g91-(xvec1epr-^*%xq$t@83i2mg3e9o=c9ib9'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'jvol',
        'USER': 'jvol',
        'PASSWORD': 'jvol',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}


INSTALLED_APPS += ["debug_toolbar"]
# MIDDLEWARE_CLASSES += 'debug_toolbar.middleware.DebugToolbarMiddleware'
