# prod.py
from conf.base import *

SECRET_KEY = env('secretkey')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env('dbname'),
        'USER': env('dbuser'),
        'PASSWORD': env('dbpass'),
        'HOST': env('dbserver'),
        'PORT': env('dbport'),
    }
}

DEBUG = False
