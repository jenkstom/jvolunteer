from django.db import models
from django.contrib.auth.models import User


class VolEvent(models.Model):
    """
    An event that a volunteer attended
    """
    #if user is logged in, put that here
    user = models.ForeignKey(User, null=True)
    #event = models.ForeignKey(Event, null=True)
    #Time can be blank but not date
    voldate = models.DateField("Date of activity")
    voltime = models.TimeField("Time of activity", null=True)
    totalhours = models.FloatField("Total Hours Volunteered")
    timestamp = models.DateTimeField("Record added", auto_now=True)
    # must have valid email to verify hours. Autofill if logged in
    emailaddr = models.EmailField("Email Address", null=False)
    name = models.CharField("Name of volunteer", max_length=100, null=False)
    childname = models.CharField("Name(s) of child(ren)", max_length=999, null=True)
    facility = models.ForeignKey(Facility)
    isEmailSent = models.BooleanField("Has verification email been sent?", default=False)
    isVerified = models.BooleanField("Hours verified by email?", default=False)
    isProcessed = models.BooleanField("Record processed?", default=False)


class Facility(models.Model):
    """
    Facilities that volunteers can volunteer at
    """
    name = models.CharField("Facility Name", max_length=100, null=False)
    addr1 = models.CharField("Address line 1", max_length=250, null=False)
    addr2 = models.CharField("Address line 2", max_length=250, null=True)
    city = models.CharField("City", max_length=100, null=False)
    state = models.CharField("State Code", max_length=2, null=False)
    zip = models.CharField("ZIP Code", max_length=10, null=False)


